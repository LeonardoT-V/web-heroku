import React from 'react'
import { TextField, Button } from '@material-ui/core'
import { useSnackbar } from 'notistack';

export default function TituloDescripcion({ setEncuesta, setMostrarTitulo, estilos, encuesta, setPreguntas, preguntas, setCambioDePregunta }) {
    const { enqueueSnackbar } = useSnackbar();

    const asignarDatos = (e) => {
        setEncuesta({
            ...encuesta,
            [e.target.name]: e.target.value
        })
    }

    const modificarEncuesta = (e) => {
        e.preventDefault();
        if (!encuesta.id_encuesta) {
            console.log("sin id prueba");
            fetch('/api/crear-encuesta', {
                method: 'POST',
                body: JSON.stringify(encuesta),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json()).then(data => {
                enqueueSnackbar(data.msg, { variant: 'success' });
                setEncuesta({
                    ...encuesta,
                    id_encuesta: data.id_encuesta
                })
                setPreguntas({
                    ...preguntas,
                    id_encuesta: data.id_encuesta
                })
            })
        } else {
            console.log("con id prueba");
            fetch(`/api/crear-encuesta/${encuesta.id_encuesta}`, {
                method: 'PUT',
                body: JSON.stringify(encuesta),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    enqueueSnackbar(data.msg, { variant: 'info' });
                })
        }
        setMostrarTitulo(false)
    }

    return (
        <>
            <form onSubmit={modificarEncuesta}>
                <TextField required id="titulo" type="text" label="Ingrese el titulo de la encuesta" defaultValue={encuesta.titulo} fullWidth/* variant="outlined"
                className={estilos.margenBot} */ name="titulo" className={estilos.margenBot} onChange={asignarDatos} />
                <TextField required id="descripcion" type="text" label="Ingrese una descripcion sobre la encuesta" defaultValue={encuesta.descripcion} fullWidth /*variant="outlined"
                className={estilos.margenBot} */ name="descripcion" className={estilos.margenBot} onChange={asignarDatos} />
                <Button variant="contained" color="primary" type="submit" className={estilos.btn}>
                    Aceptar
                </Button>
            </form>
        </>
    )
}
