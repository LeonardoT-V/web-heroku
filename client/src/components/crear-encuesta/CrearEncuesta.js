import React, { useState, useEffect } from 'react';
import Navbar from '../navbar/Navbar';
import './crearEncuesta.css';
import TituloDescripcion from './TituloDescripcion';
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles';
import FormularioEncuesta from './FormularioEncuesta';
import Preguntas from './Preguntas';
import PublicarEncuesta from './PublicarEncuesta';
import { useHistory, useParams } from 'react-router-dom'

const useStyle = makeStyles({
    margenBot: {
        marginBottom: '20px',
    },
    btn: {
        marginTop: '25px',
        padding: '10px 15px'
    },
    fullWidth: {
        width: '100%',
    },
    divisor: {
        border: '0px none black',
        borderTop: '1px solid #eeee',
        marginTop: '20px',
        marginBottom: '20px'
    },
    titulo: {
        color: '#212121',
        fontWeight: '400',
        fontSize: '20px'
    },
    preguntas: {
        color: '#757575',
        fontWeight: '300',
        fontSize: '16px',
        marginLeft: '40px',

    },
    botones: {
        display: 'flex',
        justifyContent: 'center'
    },
    formularioInput: {
        marginTop: '10px',
    },
    boton: {
        margin: '0 auto 20px auto',
        display: 'flex',
        justifyContent: 'center',

    },
    publicar: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    botonPublicar: {
        marginTop: '30px',
        color: 'white',
        backgroundColor: '#2196F3',
        '&:hover': {
            backgroundColor: '#1976D2',
        }
    },
    // ESTILO DEL COMPONENTE PREGUNTAS
    tituloBienvenido: {
        fontSize: '30px',
        textAlign: 'center',
        fontWeight: '500',
    },
    textBienvenido: {
        fontWeight: '300',
        fontSize: '15px',
        textAlign: 'center'
    },
    color: {
        color: '#689D6A'
    }
})



export default function CrearEncuesta() {
    const estilos = useStyle();
    const params = useParams();
    const history = useHistory();
    const datosUsuarios = JSON.parse(localStorage.getItem('usuario'));
    const [encuesta, setEncuesta] = useState({
        titulo: '',
        descripcion: '',
        estado: false,
        usuario: datosUsuarios.id_usuario,
    });

    const [preguntas, setPreguntas] = useState({
        titulo_pregunta: '',
        opcion_1: '',
        opcion_2: '',
        opcion_3: '',
        opcion_4: '',
    });

    const [recibirDato, setRecibirDato] = useState([])
    const [idDePregunta, setIdDePregunta] = useState(0)
    const [mostrarTitulo, setMostrarTitulo] = useState(true)
    const [editarStado, setEditarStado] = useState(false)
    const [cambioDePregunta, setCambioDePregunta] = useState(false)

    useEffect(() => {
        const conseguirEncuestaActual = () => {
            if (params.id) {
                fetch(`/api/crear-encuesta/${params.id}`)
                    .then(res => res.json())
                    .then(data => {
                        console.log(data)
                        setEncuesta({
                            titulo: data.titulo_encuesta,
                            descripcion: data.descripcion_encuesta,
                            estado: false,
                            usuario: datosUsuarios.id_usuario,
                            id_encuesta: data.id_encuesta
                        })
                        setPreguntas({
                            ...preguntas,
                            id_encuesta: data.id_encuesta
                        })
                    })
                setMostrarTitulo(false)
                setCambioDePregunta(true)
            } else {
                return
            }
        };
        const verPreguntas = () => {


            if (!encuesta.id_encuesta) {
                return
            }
            fetch(`/api/pregunta/${encuesta.id_encuesta}`)
                .then(res => res.json())
                .then(data => setRecibirDato(data));
            setCambioDePregunta(false)
        };
        conseguirEncuestaActual()
        verPreguntas()
        // eslint-disable-next-line
    }, [cambioDePregunta, params.id, encuesta.id_encuesta])


    return (
        <>
            <Navbar />
            <section className="contenedor-crear-encuesta">
                <div className="contenedor-preguntas">
                    {mostrarTitulo
                        ? <TituloDescripcion setEncuesta={setEncuesta} encuesta={encuesta} setCambioDePregunta={setCambioDePregunta} setMostrarTitulo={setMostrarTitulo} estilos={estilos} setPreguntas={setPreguntas} preguntas={preguntas} />
                        : <div>
                            <Button variant="contained" color="primary" onClick={() => setMostrarTitulo(true)}>
                                Editar titulo o descripcion
                            </Button>
                            <FormularioEncuesta setPreguntas={setPreguntas} preguntas={preguntas}
                                setEditarStado={setEditarStado}
                                editarStado={editarStado} idDePregunta={idDePregunta}
                                setCambioDePregunta={setCambioDePregunta}
                                estilos={estilos} />
                            <PublicarEncuesta
                                encuesta={encuesta} setEncuesta={setEncuesta}
                                estilos={estilos}
                            />
                        </div>
                    }

                </div>
                <main className="divisor-contenido contenedor-preguntas">
                    <Preguntas recibirDato={recibirDato} setPreguntas={setPreguntas}
                        setEditarStado={setEditarStado}
                        setIdDePregunta={setIdDePregunta}
                        preguntas={preguntas}
                        estilos={estilos}
                        setCambioDePregunta={setCambioDePregunta} />
                </main>
            </section>
            <Button variant="contained" color="primary"
                className={estilos.boton}
                size="large"
                onClick={() => history.goBack()}>
                Regresar
            </Button>
        </>
    )
}
