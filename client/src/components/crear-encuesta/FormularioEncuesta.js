import React from 'react'
import { TextField, Button } from '@material-ui/core'
import { useSnackbar } from 'notistack';


export default function FormularioEncuesta({ setPreguntas, preguntas, setCambioDePregunta, idDePregunta, setEditarStado, editarStado, estilos }) {
    const { enqueueSnackbar } = useSnackbar();
    const { titulo_pregunta, opcion_1, opcion_2, opcion_3, opcion_4 } = preguntas


    const asignarDatos = (e) => {
        setPreguntas({
            ...preguntas,
            [e.target.name]: e.target.value
        })
    }

    const agregarPregunta = (e) => {
        e.preventDefault();

        if (editarStado) {
            fetch(`/api/pregunta/${idDePregunta}`, {
                method: 'PUT',
                body: JSON.stringify(preguntas),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json()).then(data => enqueueSnackbar(data.msg, { variant: 'info' }));
            setEditarStado(false)
        } else {
            fetch('/api/pregunta', {
                method: 'POST',
                body: JSON.stringify(preguntas),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json()).then(data => enqueueSnackbar(data.msg, { variant: 'success' }))
        }
        setCambioDePregunta(true)
        setPreguntas({
            ...preguntas,
            titulo_pregunta: '',
            opcion_1: '',
            opcion_2: '',
            opcion_3: '',
            opcion_4: '',
        })
    }
    return (
        <form onSubmit={agregarPregunta}>
            <TextField
                className={estilos.formularioInput}
                id="titulo_pregunta"
                label="Ingrese el nombre para la pregunta"
                value={titulo_pregunta}
                name="titulo_pregunta"
                onChange={asignarDatos}
                fullWidth
                required
            />
            <TextField
                className={estilos.formularioInput}
                id="opcion_1"
                label="Opcion 1"
                value={opcion_1}
                name="opcion_1"
                onChange={asignarDatos}
                fullWidth
                required
            />
            <TextField
                className={estilos.formularioInput}
                id="opcion_2"
                label="Opcion 2"
                value={opcion_2}
                name="opcion_2"
                onChange={asignarDatos}
                fullWidth
                required
            />
            <TextField
                className={estilos.formularioInput}
                id="opcion_3"
                label="Opcion 3"
                value={opcion_3}
                name="opcion_3"
                onChange={asignarDatos}
                fullWidth
            />
            <TextField
                className={estilos.formularioInput}
                id="opcion_4"
                label="Opcion 4"
                value={opcion_4}
                name="opcion_4"
                onChange={asignarDatos}
                fullWidth
            />
            {!editarStado
                ? <Button variant="text" color="primary" type="submit" className={estilos.formularioInput} >
                    Agregar Pregunta
                </Button>
                : <Button variant="text" color="primary" type="submit" className={estilos.formularioInput} >
                    Editar Pregunta
                </Button>
            }
        </form>
    )
}
