import React from 'react'
import { Button, ButtonGroup, Fab, Icon } from '@material-ui/core/'
import logo from '../../img/uleam_logo.png'
import { useSnackbar } from 'notistack';


export default function Preguntas({ recibirDato, setCambioDePregunta, setPreguntas, setEditarStado, setIdDePregunta, preguntas, estilos }) {
    const { recibirDatos } = recibirDato
    const { enqueueSnackbar } = useSnackbar();

    const eliminarPregunta = (id) => {
        fetch(`/api/pregunta/${id}`, {
            method: 'DELETE',
            body: JSON.stringify(recibirDatos),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => enqueueSnackbar(data.msg, { variant: 'error' }));
        setCambioDePregunta(true)
    }
    const modificarPregunta = (id) => {
        fetch(`/api/pregunta/p/${id}`)
            .then(res => res.json())
            .then(data => {
                const { datos } = data;
                console.log(datos);
                setPreguntas({
                    ...preguntas,
                    titulo_pregunta: datos.titulo_pregunta,
                    opcion_1: datos.opcion_1,
                    opcion_2: datos.opcion_2,
                    opcion_3: datos.opcion_3,
                    opcion_4: datos.opcion_4,
                })
                setIdDePregunta(id)
                setEditarStado(true)
                setCambioDePregunta(true)
            });

    }

    return (
        <div className={estilos.fullWidth}>
            {recibirDatos
                ? <>
                    {recibirDatos.map(data => (
                        <div key={data.id_pregunta}>
                            <div>
                                <h2 className={estilos.titulo}>{data.titulo_pregunta}</h2>
                                <p className={estilos.preguntas}>{data.opcion_1}</p>
                                <p className={estilos.preguntas}>{data.opcion_2}</p>
                                <p className={estilos.preguntas}>{data.opcion_3}</p>
                                <p className={estilos.preguntas}>{data.opcion_4}</p>
                            </div>
                            <ButtonGroup variant="outlined" className={estilos.botones} size='small' >
                                <Button onClick={() => eliminarPregunta(data.id_pregunta)} color="secondary">Eliminar</Button>
                                <Button onClick={() => modificarPregunta(data.id_pregunta)} color="primary">Modificar</Button>
                            </ButtonGroup>
                            <hr className={estilos.divisor} />
                        </div>
                    ))}
                    <div className={estilos.publicar}>
                        <Fab color="primary" onClick={() => setCambioDePregunta(true)} >
                            <Icon>refresh</Icon>
                        </Fab>
                    </div>
                </>
                : (<div >
                    <h2 className={estilos.tituloBienvenido}>Bienvenido al creador de encuestas</h2>
                    <h3 className={estilos.textBienvenido}>Para empezar a crear, agregue un titulo y descripción</h3>
                    <h4 className={estilos.textBienvenido}> <span className={estilos.color}>*</span> Todos las preguntas agregadas seran almacenadas pero no publicadas, las podras modificar y eliminar cuando desees</h4>
                    <h4 className={estilos.textBienvenido}> <span className={estilos.color}>*</span> Una vez estes seguro puedes publicarla para que todos las puedan constestar pero no podra ser modificado</h4>
                    <div className={estilos.publicar}>
                        <img src={logo} alt="logo de uleam" style={{ height: '80px' }} />
                    </div>
                </div>)
            }
        </div>
    )
}
