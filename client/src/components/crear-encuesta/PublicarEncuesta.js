import React from 'react'
import { Button, Icon } from '@material-ui/core'
import { useHistory } from 'react-router-dom'

export default function PublicarEncuesta({ encuesta, estilos }) {
    const history = useHistory()
    const publicarEncuesta = (e) => {
        e.preventDefault();
        fetch(`/api/crear-encuesta/pub/${encuesta.id_encuesta}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        history.push('/')
    }

    return (
        <form onSubmit={publicarEncuesta} className={estilos.publicar}>
            <Button
                variant="contained"
                endIcon={<Icon>send</Icon>}
                type="submit"
                className={estilos.botonPublicar}
                size="large"
            >
                Publicar Encuesta
            </Button>
        </form>
    )
}
