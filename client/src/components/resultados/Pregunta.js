import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography';
import { Accordion, AccordionDetails, AccordionSummary, CircularProgress, Icon, Divider } from '@material-ui/core';
import { Chart } from "react-google-charts";

export default function Pregunta({ preguntas, estilos }) {

    const [expanded, setExpanded] = React.useState(false);
    const [estadisticas, setEstadisticas] = useState([])

    const handleChange = (panel, id) => (event, isExpanded) => {
        fetch(`/api/responder/${id}`)
            .then(res => res.json())
            .then(data => setEstadisticas(data));
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <>
            {preguntas.map((pregunta) => (
                <Accordion
                    expanded={expanded === pregunta.titulo_pregunta}
                    onChange={handleChange(pregunta.titulo_pregunta, pregunta.id_pregunta)}
                    key={pregunta.id_pregunta}
                >
                    <AccordionSummary
                        expandIcon={<Icon>expand_more</Icon>}
                    >
                        <Typography className={estilos.heading}>{pregunta.titulo_pregunta}</Typography>
                        <Typography className={estilos.secondaryHeading}>{pregunta.total_pregunta} Usuarios contestaron</Typography>
                    </AccordionSummary>
                    <Divider />
                    <AccordionDetails className={estilos.contenedorGrafico}>
                        {estadisticas
                            ? <Chart
                                width={'500px'}
                                height={'300px'}
                                chartType="PieChart"
                                loader={<CircularProgress />}
                                data={estadisticas}
                            />
                            : (<h2>no hay resultados</h2>)}

                    </AccordionDetails>
                </Accordion>
            ))}
        </>
    );
}
