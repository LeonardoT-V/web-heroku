import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import Navbar from '../navbar/Navbar';
import Pregunta from './Pregunta';
import { Button, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import logo from '../../img/uleam_logo.png'
const useStyles = makeStyles({
    contenedor: {
        width: '50%',
        margin: '20px auto 0px auto',
        padding: '20px',
        textAlign: 'center'
    },
    titulo: {
        fontSize: '30px',
        fontWeight: '400'
    },
    descripcion: {
        fontSize: '15px',
        fontWeight: '300'
    },
    contendorAcordeon: {
        width: '80%',
        margin: '50px auto'
    },
    heading: {
        flexBasis: '33.33%',
        flexShrink: 0,
        textTransform: 'capitalize'
    },
    secondaryHeading: {
        color: '#BDBDBD'
    },
    contenedorGrafico: {
        display: 'flex',
        justifyContent: 'center'
    },
    boton: {
        margin: 'auto',
        display: 'flex',
        justifyContent: 'center'
    },

    error: {
        width: '40%',
        margin: 'auto',
        padding: '20px 50px',
        textAlign: 'center',
        '& h2': {
            fontSize: '20px',
            fontWeight: '400'
        },
        '& h3': {
            fontSize: '10px',
            fontWeight: '300'
        }
    }
});

export default function Resultado() {
    const params = useParams();
    const estilos = useStyles();
    const history = useHistory();
    const [preguntas, setPreguntas] = useState([])
    useEffect(() => {
        const mostrarPreguntas = () => {
            fetch(`/api/responder/r/${params.id}`)
                .then(res => res.json())
                .then(data => setPreguntas(data.recibirDatos))
        }
        mostrarPreguntas();
    }, [params.id])
    return (
        <div>
            <Navbar />
            <Paper square >
                <div className={estilos.contenedor}>
                    <h2 className={estilos.titulo}>Visualiza los datos de la encuesta</h2>
                    <h3 className={estilos.descripcion}>Usa los datos recogidos de los usuarios para tus proyectos</h3>
                </div>
            </Paper>
            <div className={estilos.contendorAcordeon} >
                {preguntas.length !== 0
                    ? (<Pregunta
                        preguntas={preguntas}
                        estilos={estilos}
                    />)
                    : (<Paper className={estilos.error}>

                        <h2>Aun no hay respuestas de la comunidad</h2>
                        <h3>Intentelo de nuevo mas tarde o cuando se haya contestado almenos 1 vez</h3>
                        <img src={logo} alt="logo de uleam" style={{ height: '80px' }} />
                    </Paper>)}
            </div>
            <Button variant="contained" color="primary"
                className={estilos.boton}
                size="large"
                onClick={() => history.goBack()}>
                Regresar
            </Button>
        </div>
    )
}
