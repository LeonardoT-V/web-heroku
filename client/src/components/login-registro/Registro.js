import React, { useState } from 'react'
import logo from '../../img/uleam_logo.png'
import { Link, useHistory } from 'react-router-dom' /* modulo para la creacion de rutas */
import './login_registro.css'
import { Button, TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles({

    margenBot: {
        marginBottom: '10px',
    },
    btn: {
        marginTop: '25px',
        fontWeight: '300',
        textTransform: 'none',
        padding: '10px 15px'
    }
})

export default function Registro() {
    const estilos = useStyle()
    let history = useHistory()
    const [formulario, setFormulario] = useState({
        nombre: '',
        password: '',
        email: ''
    })

    const [validarNombre, setValidarNombre] = useState({
        mensaje: 'Ingrese sus datos',
        estado: false
    })
    const [validarContrasena, setValidarContrasena] = useState({
        mensaje: 'Ingrese una contraseña valida',
        estado: false
    })
    const [validarCorreo, setValidarCorreo] = useState({
        mensaje: 'Asegurese de tener acceso',
        estado: false
    })

    const asignarDatos = (e) => {
        setFormulario({
            ...formulario,
            [e.target.name]: e.target.value
        })
    }

    const registrarCuenta = (e) => {
        e.preventDefault();
        setValidarNombre({
            mensaje: '',
            estado: false
        })
        setValidarContrasena({
            mensaje: '',
            estado: false
        })
        setValidarCorreo({
            mensaje: '',
            estado: false
        })
        if (formulario.nombre.trim().length === 0) {
            setValidarNombre({
                mensaje: 'Ingrese un nombre valido',
                estado: true
            })
            return;
        }

        if (formulario.password.length < 6) {
            setValidarContrasena({
                mensaje: 'Ingrese contraseña valida',
                estado: true
            })
            return;
        }

        if (formulario.email.trim().length === 0) {
            setValidarCorreo({
                mensaje: 'Ingrese un correo valido',
                estado: true
            })
            return;
        }



        fetch('/api/registro', {
            method: 'POST',
            body: JSON.stringify(formulario),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json()).then(data => {
                const { msg, body } = data
                /*  console.log(data);
                 console.log(msg); */
                if (msg === 'correo se encuentra usado') {
                    setValidarCorreo({
                        mensaje: msg,
                        estado: true
                    })
                    return;
                }
                localStorage.setItem("usuario", JSON.stringify(body))
                console.log("creado");
                history.push('/')
            })
    }

    return (
        <>
            <div className="fondo">
                <div className="form">
                    <form className="login-form" onSubmit={registrarCuenta}>
                        <div className="login-header">
                            <Link to={"/"}><img src={logo} alt="logo uleam" height="100px" /></Link>
                            <h2>Registrate</h2>
                        </div>
                        <TextField id="nombre" type="text" label="Ingrese su nombre y apellido" defaultValue='' fullWidth variant="outlined"
                            className={estilos.margenBot} margin="dense"
                            name="nombre" onChange={asignarDatos}
                            helperText={validarNombre.mensaje}
                            error={validarNombre.estado}
                        />
                        <TextField id="password" type="password" label="Ingrese su contraseña" defaultValue='' fullWidth variant="outlined"
                            className={estilos.margenBot} helperText={validarContrasena.mensaje} margin="dense" error={validarContrasena.estado}
                            name="password" onChange={asignarDatos}
                        />
                        <TextField id="email" type="email" label="Ingrese su correo electronico" defaultValue='' fullWidth variant="outlined"
                            className={estilos.margenBot} helperText={validarCorreo.mensaje} margin="dense"
                            error={validarCorreo.estado}
                            name="email" onChange={asignarDatos}
                        />
                        <Button variant="contained" disableElevation type="submit" >
                            Registrate
                        </Button>
                        <p className="message">¿Tienes una cuenta? <Link to="/login">Iniciar sesión</Link></p>
                        <p className="message"><Link to="/">Regresar</Link></p>
                    </form>
                </div>
            </div>
        </>
    )
}
