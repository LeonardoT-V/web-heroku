import React, { useState } from 'react'
import logo from '../../img/uleam_logo.png'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link, useHistory } from 'react-router-dom' /* modulo para la creacion de rutas */
import './login_registro.css'
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles({

    margenBot: {
        marginBottom: '20px',
    },
    btn: {
        marginTop: '25px',
        fontWeight: '300',
        textTransform: 'none',
        padding: '10px 15px'
    }
})

export default function Login() {
    const estilos = useStyle()
    let history = useHistory()

    const [formulario, setFormulario] = useState({
        email: '',
        pass: ''
    })
    const [validarContrasena, setValidarContrasena] = useState({
        mensaje: '',
        estado: false
    })
    const [validarCorreo, setValidarCorreo] = useState({
        mensaje: '',
        estado: false
    })
    const asignarDatos = (e) => {
        setFormulario({
            ...formulario,
            [e.target.name]: e.target.value
        })
    }
    const loginDeCuenta = (e) => {
        e.preventDefault()

        setValidarCorreo({
            mensaje: '',
            estado: false
        })
        setValidarContrasena({
            mensaje: '',
            estado: false
        })

        fetch('/api/auth', {
            method: 'POST',
            body: JSON.stringify(formulario),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            console.log(data)
            const { msg, body } = data

            if (msg === 'correo incorrecto') {
                setValidarCorreo({
                    mensaje: msg,
                    estado: true
                })
                return;
            }
            if (msg === 'contraseña incorrecta') {
                setValidarContrasena({
                    mensaje: msg,
                    estado: true
                })
                return;
            }

            localStorage.setItem("usuario", JSON.stringify(body))
            console.log("logeado correctamente");
            history.push('/')
        })
    }

    return (
        < >
            <div className="fondo ">
                <div className="form">
                    <form className="login-form" onSubmit={loginDeCuenta}>
                        <div className="login-header">
                            <Link to={"/"}><img src={logo} alt="logo uleam" height="100px" /></Link>
                            <h2>Login</h2>
                        </div>
                        <TextField required id="email" type="email" label="Ingrese su correo" defaultValue={formulario.email} fullWidth variant="outlined"
                            className={estilos.margenBot} name="email" onChange={asignarDatos}
                            helperText={validarCorreo.mensaje} error={validarCorreo.estado} />
                        <TextField required id="pass" type="password" label="Ingresa tu contraseña" defaultValue={formulario.password} fullWidth variant="outlined"
                            className={estilos.margenBot} name="pass" onChange={asignarDatos}
                            helperText={validarContrasena.mensaje} error={validarContrasena.estado} />
                        <Button variant="contained" disableElevation type="submit" >
                            Iniciar Sesion
                        </Button>
                        <p className="message">¿No tiene una cuenta? <Link to="/registro">Cree una</Link></p>
                        <p className="message"><Link to="/">Regresar</Link></p>
                    </form>
                </div>
            </div>
        </>
    )
}
