import React from 'react'
import './contacto.css'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Navbar from '../navbar/Navbar'
import { makeStyles } from '@material-ui/core/styles';
import Map from '../apiMapa/Map';
const useStyle = makeStyles({

    margenBot: {
        marginBottom: '8px',
    },
    btn: {
        marginTop: '25px',
        fontWeight: '300',
        textTransform: 'none',
        padding: '10px 15px',
        marginBottom: '20px'
    }
})

export default function Contacto() {

    const estilos = useStyle()

    const [formulario, setFormulario] = useState({
        nombre: '',
        correo: '',
        telefono: '',
        mensaje: '',
        problema: ''
    });
    const asignarDatos = (e) => {
        setFormulario({
            ...formulario,
            [e.target.name]: e.target.value
        })
    }
    const publicarProblema = (e) => {
        e.preventDefault();
        fetch('http://localhost:4000/api/contactanos', {
            method: 'POST',
            body: JSON.stringify(formulario),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => enqueueSnackbar(data.msg, { variant: 'success' }));
        setFormulario({
            nombre: '',
            correo: '',
            telefono: '',
            mensaje: '',
            problema: ''
        })
    }

    return (
        <>
            <Navbar />
            <header>
                <div className="fondo-titulo">
                    <h1>Contactate con nosotros</h1>
                    <div>
                        <p>Tiene preguntas o problemas con tus encuestas en línea? Por favor no dude en contactar con nosotros!
                            <br />
                            Llena el formulario o visita nuestras redes
                        </p>
                    </div>
                    <div className="btn-contenedor">
                        <Button variant="contained" size="large" color="primary" type="submit" className={estilos.btn} href="#formulario">
                            Formulario
                        </Button>
                        <Button variant="contained" size="large" color="primary" type="submit" className={estilos.btn} href="#redes">
                            Redes Sociales
                        </Button>
                    </div>
                </div>
            </header>

            <hr className="line-divisor" />

            <section className="contenedor-form">
                <div className="espacio-form">
                    <h2 id="formulario" className="color-principal">Llene el formulario</h2>
                    <form className="llenar-form" onSubmit={publicarProblema}>
                        <TextField required name="nombre" type="text" label="Nombre" value={formulario.nombre}
                            onChange={asignarDatos}
                            className={estilos.margenBot} />
                        <TextField required name="correo" type="email" label="Correo Electronico" value={formulario.correo}
                            onChange={asignarDatos}
                            className={estilos.margenBot} />
                        <TextField required name="telefono" type="tel" label="Numero telefonico" value={formulario.telefono}
                            onChange={asignarDatos}
                            className={estilos.margenBot} />
                        <TextField required name="mensaje" type="text" label="Motivo del mensaje" value={formulario.mensaje}
                            onChange={asignarDatos}
                            className={estilos.margenBot} />
                        <TextField required multiline type="text" name="problema" label="Escribanos su duda o problema" value={formulario.problema}
                            minRows={3}
                            onChange={asignarDatos}
                        />

                        <Button variant="contained" color="primary" disableElevation type="submit" className={estilos.btn}>
                            Enviar
                        </Button>
                    </form>
                </div>
                <div className="espacio-form">
                    <h2 id="redes" className="color-principal">O visite nuestras redes</h2>
                    <div>
                        <h3>Nuestro Correo</h3>
                        <p>contacto@uleam.edu.ec</p>
                    </div>
                    <div>
                        <h3>Redes Sociales</h3>
                        <div className="redes-contenedor">
                            <a href="https://www.facebook.com/UleamEc" className="facebook"><i className="fab fa-facebook-f"></i></a>
                            <a href="https://twitter.com/UleamEcuador" className="twitter"><i className="fab fa-twitter"></i></a>
                            <a href="https://instagram.com" className="instagram"><i className="fab fa-instagram"></i></a>
                            <a href="https://www.youtube.com/channel/UCOferC0PjpEUegrVq8Re-TQ" className="youtube"><i
                                className="fab fa-youtube"></i></a>
                        </div>
                    </div>
                    <p className="centrar-texto">No dudes en escribirnos entre las 8am y 7pm</p>
                </div>
            </section>

            <Map />

            <footer className="footer">
                <p>ULEAM © Copyright 2020, Todos los derechos reservados - Universidad Laica Eloy Alfaro de Manabí </p>
                <p>Dirección: Av. Circunvalación - Vía a San Mateo </p>
                <p>Manta - Manabí - Ecuador </p>
            </footer>

        </>
    )
}
