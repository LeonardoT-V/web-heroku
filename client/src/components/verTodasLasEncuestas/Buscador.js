import React, { useState } from 'react'
import { Paper, InputBase, Icon, IconButton, Divider } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 600,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));

export default function Buscador({ setEncuestas }) {
    const estilos = useStyles();
    const [buscador, setBuscador] = useState({ titulo: '' })
    const asignarDatos = (e) => {
        setBuscador({
            ...buscador,
            [e.target.name]: e.target.value
        })
    }
    const buscarEncuesta = (e) => {
        e.preventDefault();
        if (buscador.titulo === '') {
            fetch('/api/encuesta')
                .then(res => res.json())
                .then(data => setEncuestas(data));
        } else {
            fetch(`/api/encuesta/search/${buscador.titulo}`)
                .then(res => res.json())
                .then(data => setEncuestas(data));
        }

    }
    return (
        <>
            <Paper component="form" onSubmit={buscarEncuesta} className={estilos.root}>
                <InputBase
                    placeholder="Buscar una encuesta"
                    id="titulo"
                    name="titulo"
                    value={buscador.buscador}
                    onChange={asignarDatos}
                    className={estilos.input}
                />
                <Divider className={estilos.divider} orientation="vertical" />
                <IconButton type="submit" className={estilos.iconButton}>
                    <Icon>search</Icon>
                </IconButton>
            </Paper>
        </>
    )
}
