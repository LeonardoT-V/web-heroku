import React, { useEffect, useState } from 'react'
import Navbar from '../navbar/Navbar'
import { useHistory } from 'react-router-dom'
import { Card, CardActions, CardContent, CardHeader, Button, Avatar, Paper, Fab, Icon } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import Buscador from './Buscador';

const useStyle = makeStyles({
    grid: {
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill,minmax(18rem, 1fr))',
        gap: '1rem',
        margin: '20px 50px',
    },
    cabecera: {
        border: '1px none black',
        borderBottom: '1px solid #eeee'
    },
    titulo: {
        color: '#212121',
        margin: '0px 20px',
        fontSize: '18px',
        fontWeight: '500',
        textTransform: 'capitalize'
    },
    descripcion: {
        color: '#757575',
        fontWeight: '300',
        fontSize: '15px',
    },
    contenedor: {
        display: 'flex',
        justifyContent: 'center',
        margin: '30px 50px 20px 50px'
    },
    borde: {
        height: '3px',
        margin: '0px',
        background: '#1976d2'
    },
    contenedorTitulo: {
        width: '50%',
        margin: '20px auto 0px auto',
        padding: '20px',
        textAlign: 'center'
    },
    tituloContenedor: {
        fontSize: '30px',
        fontWeight: '400'
    },
    descripcionContenedor: {
        fontSize: '15px',
        fontWeight: '300'
    }

})


export default function MostrarEncuestas() {
    const estilos = useStyle()
    const moment = window.moment;
    const history = useHistory();
    const [actualizar, setActualizar] = useState(true);
    const [encuestas, setEncuestas] = useState([]);

    useEffect(() => {
        const actualizarListaEncuestas = () => {
            if (actualizar) {
                fetch('/api/encuesta')
                    .then(res => res.json())
                    .then(data => setEncuestas(data));
                setActualizar(false)
            }
        }
        actualizarListaEncuestas()
    }, [actualizar])

    const irResponderPregunta = (id) => {
        history.push(`/responder/${id}`);
    }
    return (
        <>
            <Navbar />
            <Paper square>
                <div className={estilos.contenedorTitulo}>
                    <h2 className={estilos.tituloContenedor} >Encuestas de la Comunidad</h2>
                    <h3 className={estilos.descripcionContenedor} >Contesta las encuestas desarrolladas por la comunidad y ayudalos en sus trabajos de investigacion o tareas</h3>
                </div>
            </Paper>
            <div className={estilos.contenedor} >
                <Buscador setEncuestas={setEncuestas} estilos={estilos} />
            </div>
            <div className={estilos.contenedorTitulo} >
                <Fab color="primary" onClick={() => setActualizar(true)}  >
                    <Icon>refresh</Icon>
                </Fab>
            </div>

            {!encuestas.length
                ?
                <Card className={estilos.contenedor} style={{ padding: '100px 0px' }}>
                    <h3 style={{ fontSize: '30px' }}>NO SE ENCONTRARON ENCUESTAS</h3>
                </Card>
                :
                <div className={estilos.grid}>
                    {encuestas.map(enc => (
                        <Card key={enc.id_encuesta} >
                            <div className={estilos.borde}></div>
                            <CardHeader
                                title={enc.nombre_usuario}
                                avatar={<Avatar variant='rounded'>{enc.nombre_usuario[0].toUpperCase()}</Avatar>}
                                subheader={moment(enc.fecha_publicado_encuesta).format('llll')}
                                className={estilos.cabecera}
                            />
                            <CardContent>
                                <h3 className={estilos.titulo}>{enc.titulo_encuesta}</h3>
                                <p className={estilos.descripcion}>{enc.descripcion_encuesta}</p>
                            </CardContent>
                            <CardActions>
                                <Button size="small" variant="contained" color="primary" onClick={() => irResponderPregunta(enc.id_encuesta)}>Responder</Button>
                            </CardActions>
                        </Card>
                    ))
                    }
                </div>}
        </>
    )
}
