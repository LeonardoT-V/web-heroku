import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';


const useStyle = makeStyles({

    mapa: {
        width: '50rem',
        height: '25rem'
    },
    contenedorMapa: {
        display: 'flex',
        justifyContent: 'center'
    }
})

export default function Map() {
    let google = window.google;
    let estilos = useStyle()
    useEffect(() => {
        const iniciarMapa = () => {
            new google.maps.Map(document.getElementById("map"), {
                center: { lat: -0.9533683, lng: -80.7456925 },
                zoom: 16,
            })
        }
        iniciarMapa()
        // eslint-disable-next-line
    }, [])



    return (
        <div className={estilos.contenedorMapa}>
            <div id="map" className={estilos.mapa}>
            </div>
        </div>

    )
}
