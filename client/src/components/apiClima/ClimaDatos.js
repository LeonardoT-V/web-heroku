import React from 'react'

export default function ClimaDatos({ clima, estilos }) {
    if (Object.keys(clima).length === 0) {
        return (
            <h2>Estamos viendo el clima...</h2>
        )
    }
    const { main, name, sys, weather } = clima;
    const kelvin = 273.15;
    return (
        <div>
            {clima
                ?
                (<>
                    <h2 className={estilos.titulo} >El clima en {name}, {sys.country}</h2>
                    <p>Con una temperatura de {parseFloat(main.temp - kelvin, 10).toFixed(2)}<span>&#x2103;</span> </p>
                    <div className={estilos.contenedorDescripcion}>
                        <div>
                            <p>Cielo : {weather[0].main}</p>
                            <p>descripcion: {weather[0].description}</p>
                        </div>
                        <div>
                            <img className={estilos.icono} src={`http://openweathermap.org/img/w/${weather[0].icon}.png`} alt="icon climas" />
                        </div>
                    </div>

                </>)
                : null}
        </div>
    )
}
