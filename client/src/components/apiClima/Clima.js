import React, { useEffect, useState } from 'react'
import ClimaDatos from './ClimaDatos'
import { makeStyles } from '@material-ui/core/styles';


const useStyle = makeStyles({
    contenedorClima: {
        padding: '20px',
        textAlign: 'center'
    },
    titulo: {
        textAlign: 'center',
    },
    icono: {
        height: '80px',
        display: 'flex',
        justifyContent: 'center',
        margin: 'auto',
    },
    contenedorDescripcion: {
        display: 'flex',
        justifyContent: 'space-around'
    }
});

export default function Clima() {
    const [clima, setClima] = useState({})

    useEffect(() => {
        const consultarAPI = async () => {
            const apiKey = '99e2c89ff648831ce269c2f32918fecf';
            const url = `https://api.openweathermap.org/data/2.5/weather?q=manta,ec&appid=${apiKey}`;
            const respuesta = await fetch(url);
            const resultado = await respuesta.json();
            setClima(resultado)
        }
        consultarAPI();
    }, [])



    return (
        <div className={estilos.contenedorClima}>
            <ClimaDatos clima={clima} estilos={estilos} />
        </div>
    )
}
