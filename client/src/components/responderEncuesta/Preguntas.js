import React from 'react'
import { Radio, RadioGroup, FormControlLabel, Paper } from '@material-ui/core'
export default function Preguntas({ preguntas, setRespuestas, respuestas, estilos }) {

    const seleccionarOpcion = (e, id) => {
        setRespuestas({
            ...respuestas,
            [id]: e.target.value
        })
    }

    return (
        <>
            {preguntas.map((pregunta, index) => (
                <Paper key={pregunta.id_pregunta} elevation={0} className={estilos.contenedorPregunta}>
                    <h2 className={estilos.pregunta}> <span>{index + 1}.</span> {pregunta.titulo_pregunta}</h2>
                    <RadioGroup name={pregunta.titulo_pregunta} onChange={(e) => { seleccionarOpcion(e, pregunta.id_pregunta) }}
                        className={estilos.radios}
                    >
                        <FormControlLabel value={pregunta.opcion_1} control={<Radio />} label={pregunta.opcion_1} />
                        <FormControlLabel value={pregunta.opcion_2} control={<Radio />} label={pregunta.opcion_2} />
                        {pregunta.opcion_3 !== '' ? <FormControlLabel value={pregunta.opcion_3} control={<Radio />} label={pregunta.opcion_3} />
                            : null}
                        {pregunta.opcion_4 !== '' ? <FormControlLabel value={pregunta.opcion_4} control={<Radio />} label={pregunta.opcion_4} />
                            : null}
                    </RadioGroup>
                </Paper>
            ))
            }
        </>
    )
}
