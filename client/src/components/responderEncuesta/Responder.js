import React, { useEffect, useState } from 'react'
import Preguntas from './Preguntas'
import { useHistory, useParams } from 'react-router-dom'
import { Button, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles({
    contenedor: {
        width: '80%',
        margin: 'auto'
    },
    contenedorTitulo: {
        textAlign: 'center',
        padding: '20px 50px',
        backgroundColor: '#C8E6C9',
    },
    titulo: {
        fontSize: '30px'
    },
    descripcion: {
        fontSize: '15px',
        fontWeight: '300'
    },
    contenedorPregunta: {
        width: '80%',
        margin: '50px auto'
    },
    pregunta: {
        color: '#388E3C',
        textTransform: 'capitalize'
    },
    radios: {
        marginLeft: '50px'
    }
})


export default function Responder() {
    const params = useParams();
    const history = useHistory();
    const estilos = useStyle();
    const [preguntas, setPreguntas] = useState([]);
    const [respuestas, setRespuestas] = useState({})
    const [datoEncuesta, setDatoEncuesta] = useState({})
    useEffect(() => {
        const mostrarPreguntas = () => {
            fetch(`/api/pregunta/${params.id}`)
                .then(res => res.json())
                .then(data => setPreguntas(data.recibirDatos))
        }
        const recibirDatoDeEncuesta = () => {
            fetch(`/api/crear-encuesta/${params.id}`)
                .then(res => res.json())
                .then(data => setDatoEncuesta(data))
        }
        recibirDatoDeEncuesta();
        mostrarPreguntas();
    }, [params.id]);

    const subirRespuestas = (e) => {
        e.preventDefault();
        fetch(`/api/responder/${params.id}`, {
            method: 'POST',
            body: JSON.stringify(respuestas),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        history.push('/')
    }

    return (
        <Paper square elevation={1} className={estilos.contenedor}>
            <form onSubmit={subirRespuestas}>
                <Paper variant="outlined" square className={estilos.contenedorTitulo}>
                    <h2 className={estilos.titulo}>{datoEncuesta.titulo_encuesta}</h2>
                    <h3 className={estilos.descripcion}>{datoEncuesta.descripcion_encuesta}</h3>
                </Paper>
                <Preguntas
                    preguntas={preguntas}
                    respuestas={respuestas}
                    setRespuestas={setRespuestas}
                    estilos={estilos}
                />
                <Paper variant="outlined" square className={estilos.contenedorTitulo}>
                    <Button variant="contained" color="primary" type="submit" size="large">
                        Responder Encuesta
                    </Button>
                </Paper>

            </form>
        </Paper>
    )
}
