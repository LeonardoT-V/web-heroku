import React from 'react'
import { Link, NavLink, useHistory } from 'react-router-dom' /* modulo para la creacion de rutas */
import { MenuItem, Avatar, Menu, AppBar } from '@material-ui/core'
import logo from '../../img/logo-uleam-nav.png'

export default function Navbar() {
    const datosUsuarios = JSON.parse(localStorage.getItem('usuario'))
    const history = useHistory()

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const cerrarSesion = () => {
        setAnchorEl(null);
        localStorage.clear()
        history.push('/')
    }
    const goToMisEncuestas = () => {
        setAnchorEl(null);
        history.push('/mis-encuestas')
    }

    const goToEncuestas = () => {
        setAnchorEl(null);
        history.push('/encuestas')
    }
    const goToCrearEncuesta = () => {
        setAnchorEl(null);
        history.push('/crear-encuesta')
    }
    return (
        <>
            <AppBar position="sticky" color="inherit" >

                <nav className="contenedor-navegacion">
                    <div className="navegacion-pages">
                        <Link to="/"><img src={logo} alt="logo de uleam" height="55px" /></Link>
                    </div>
                    <div className="navegacion-pages">
                        <NavLink exact to="/" className="enlace-link nav-hover" activeClassName="navegacion-active">Inicio</NavLink>
                        <NavLink to="/contactanos" className="enlace-link nav-hover" activeClassName="navegacion-active">Contactanos</NavLink>
                        <NavLink to="/nosotros" className="enlace-link nav-hover" activeClassName="navegacion-active">Nosotros</NavLink>
                    </div>
                    {localStorage.getItem('usuario')
                        ? <div className="navegacion-pages">

                            <h2 className="enlace-link">{datosUsuarios.email}</h2>
                            <Avatar style={{ background: '#689D6A' }} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>{datosUsuarios.nombre_usuario[0].toUpperCase()}</Avatar>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={goToCrearEncuesta}>Crear encuestas</MenuItem>
                                <MenuItem onClick={goToMisEncuestas}>Mis encuestas</MenuItem>
                                <MenuItem onClick={goToEncuestas}>Ver Encuestas</MenuItem>
                                <MenuItem onClick={cerrarSesion}>Cerrar Sesion</MenuItem>
                            </Menu>
                        </div>
                        : <div className="navegacion-pages">
                            <Link to="/login" className="navegacion-borde enlace-link navegacion-login">Login</Link>
                            <Link to="/registro" className="navegacion-borde navegacion-registro enlace-link">Registro</Link>
                        </div>
                    }
                </nav>
            </AppBar>
        </>
    )
}
