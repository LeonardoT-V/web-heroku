import React, { useEffect, useState } from 'react';
import Navbar from '../navbar/Navbar';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Button, Fab, Icon } from '@material-ui/core'
import MiEncuesta from './MiEncuesta';
import { useHistory } from 'react-router-dom';
const useStyle = makeStyles({
    grid: {
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill,minmax(18rem,1fr))',
        gap: '1rem',
        gridAutoRows: '22rem',
        margin: '20px 50px',
    },
    estadoPublicado: {
        backgroundColor: '#0288D1'
    },
    estadoPendiente: {
        backgroundColor: '#D32F2F'
    },
    padCard: {
        padding: '0px',
        height: '90px',
    },
    textEstado: {
        fontSize: '30px',
        color: 'white',
        margin: '0',
        padding: '10px',
        textAlign: 'center',
    },
    contenedor: {
        width: '50%',
        margin: '20px auto 0px auto',
        padding: '20px',
        textAlign: 'center'
    },
    titulo: {
        fontSize: '30px',
        fontWeight: '400'
    },
    descripcion: {
        fontSize: '15px',
        fontWeight: '300'
    },
    contenedorFab: {
        width: '50%',
        margin: '20px auto 0px auto',
        textAlign: 'center'
    },
})

export default function MisEncuestas() {
    const estilos = useStyle();
    const history = useHistory();
    const [encuestas, setEncuestas] = useState([])
    const [actualizar, setActualizar] = useState(true)
    useEffect(() => {
        const actualizarListaEncuestas = () => {
            const datosUsuarios = JSON.parse(localStorage.getItem('usuario'))
            if (actualizar) {
                fetch(`/api/encuesta/mis-encuestas/${datosUsuarios.id_usuario}`)
                    .then(res => res.json())
                    .then(data => setEncuestas(data));
                setActualizar(false)
            }
        }
        actualizarListaEncuestas()
    }, [actualizar, encuestas])
    return (
        <>
            <Navbar />
            <Paper square>
                <div className={estilos.contenedor}>
                    <h2 className={estilos.titulo}>Mis Encuestas</h2>
                    <h3 className={estilos.descripcion}>Revisa como van tus encuestas y publica encuestas para que la comunidad las conteste</h3>
                    <Button variant="contained" color="primary"
                        onClick={() => history.push('/crear-encuesta')}>
                        Crea una nueva encuesta
                    </Button>
                </div>
            </Paper>
            <div className={estilos.contenedorFab}>
                <Fab color="primary"
                    onClick={() => {
                        setActualizar(true);
                    }}>
                    <Icon>refresh</Icon>
                </Fab>
            </div>

            <MiEncuesta encuestas={encuestas} estilos={estilos} setActualizar={setActualizar} />
        </>
    )
}
