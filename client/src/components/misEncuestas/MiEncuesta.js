import React from 'react'
import { Card, CardActions, CardContent, CardHeader, Button, Divider } from '@material-ui/core/';
import { useHistory } from 'react-router-dom'
import { useSnackbar } from 'notistack';

export default function MiEncuesta({ encuestas, estilos, setActualizar }) {
    const { enqueueSnackbar } = useSnackbar();
    const moment = window.moment;
    const history = useHistory();
    const eliminarEncuesta = (id) => {
        fetch(`/api/crear-encuesta/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => enqueueSnackbar(data.msg, { variant: 'error' }));
        setActualizar(true)
    }
    const publicarEncuesta = (id) => {
        fetch(`/api/crear-encuesta/pub/${id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => enqueueSnackbar(data.msg, { variant: 'info' }));
        setActualizar(true)
    }
    return (
        <div className={estilos.grid}>
            {encuestas.map(enc => (
                <Card key={enc.id_encuesta}>
                    <CardHeader
                        subheader={moment(enc.fecha_publicado_encuesta).format('llll')}
                    />
                    {enc.estado_encuesta
                        //eslint-disable-next-line
                        ? <CardContent className={estilos.padCard, estilos.estadoPublicado}>
                            <p className={estilos.textEstado}>Estado: Publicado</p>
                        </CardContent>
                        // eslint-disable-next-line
                        : <CardContent className={estilos.padCard, estilos.estadoPendiente}>
                            <p className={estilos.textEstado}>Estado: Pendiente</p>
                        </CardContent>}

                    <CardContent>
                        <h3 >{enc.titulo_encuesta}</h3>
                        <p>{enc.descripcion_encuesta}</p>
                    </CardContent>
                    <Divider />
                    <CardActions>
                        {enc.estado_encuesta
                            ? (<Button size="small" color="primary" onClick={() => history.push(`/resultado/${enc.id_encuesta}`)} >Ver resultados</Button>)
                            : (<>
                                <Button size="small" onClick={() => history.push(`/editar/${enc.id_encuesta}`)}>Editar</Button>
                                <Button size="small" color="primary" onClick={() => publicarEncuesta(enc.id_encuesta)}>Publicar</Button>
                            </>)}
                        <Button size="small" color="secondary" onClick={() => eliminarEncuesta(enc.id_encuesta)} >Eliminar</Button>
                    </CardActions>
                </Card>
            ))}
        </div>
    )
}
