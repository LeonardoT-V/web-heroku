import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom' /* modulo para la creacion de rutas */
import Inicio from './components/inicio/Inicio'
import Contacto from "./components/contactanos/Contacto";
import Nosotros from "./components/nosotros/Nosotros";
import Registro from "./components/login-registro/Registro";
import Login from "./components/login-registro/Login";
import CrearEncuesta from './components/crear-encuesta/CrearEncuesta';
import theme from './themeConfig'

import { ThemeProvider } from '@material-ui/core/styles';
import { SnackbarProvider } from 'notistack';
import MisEncuestas from './components/misEncuestas/MisEncuestas';
import MostrarEncuestas from './components/verTodasLasEncuestas/MostrarEncuestas';
import Responder from './components/responderEncuesta/Responder';
import Resultado from './components/resultados/Resultado';
import * as serviceWorker from './serviceWorker';


ReactDOM.render(
  <>
    <ThemeProvider theme={theme}>
      <SnackbarProvider autoHideDuration={3000} hideIconVariant dense>
        <Router>
          <Switch>
            <Route exact path="/" component={Inicio}>
              {/* <Inicio /> */}
            </Route>
            <Route exact path="/contactanos" component={Contacto}>
              {/* <Contacto /> */}
            </Route>
            <Route path="/nosotros" component={Nosotros}>
              {/* <Nosotros /> */}
            </Route>
            <Route exact path="/login" component={Login}>
              {/* <Login /> */}
            </Route>
            <Route exact path="/registro" component={Registro}>
              {/* <Registro /> */}
            </Route>
            <Route exact path="/crear-encuesta" component={CrearEncuesta}>
              {/* <CrearEncuesta /> */}
            </Route>
            <Route exact path="/mis-encuestas" component={MisEncuestas}>
              {/* <MisEncuestas /> */}
            </Route>
            <Route exact path="/encuestas" component={MostrarEncuestas}>
              {/* <MostrarEncuestas /> */}
            </Route>
            <Route path="/editar/:id" component={CrearEncuesta}>
            </Route>
            <Route path="/responder/:id" component={Responder}>
            </Route>
            <Route path="/resultado/:id" component={Resultado}>
            </Route>

          </Switch>
        </Router >
      </SnackbarProvider>
    </ThemeProvider>
  </>,
  document.getElementById('root')
);
serviceWorker.register();
