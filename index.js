const express = require('express')
const cors = require('cors')
const app = express();
const path = require('path')

//middlewares
app.use(express.json())
app.use(cors())

//Directorio publico
if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, "client/build")))
}

//routes
app.use('/api/auth', require('./src/routes/auth.routes'))
app.use('/api/registro', require('./src/routes/registroUsuario.routes'))
app.use('/api/crear-encuesta', require('./src/routes/crearEncuesta.routes'))
app.use('/api/pregunta', require('./src/routes/pregunta.routes'))
app.use('/api/encuesta', require('./src/routes/encuestas.routes'))
app.use('/api/responder', require('./src/routes/responderEncuesta.routes'))
app.use('/api/contactanos', require('./routes/contactanos.routes'))


app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "client/build/index.html"))
})

const PORT = process.env.PORT || 4000;



app.listen(PORT, () => {
    console.log("escuchando puerto " + PORT);
});