
CREATE TABLE Usuario(
    id_usuario SERIAL PRIMARY KEY,
    nombre_usuario TEXT,
    email_usuario TEXT UNIQUE,
	password_usuario TEXT,
    fecha_ingreso_usuario date default now()
);


CREATE TABLE Encuesta(
    id_encuesta SERIAL PRIMARY KEY,
    id_usuario_encuesta INTEGER,
    fecha_publicado_encuesta TIMESTAMP default now(),
    titulo_encuesta TEXT,
    descripcion_encuesta TEXT,
    estado_encuesta BOOLEAN,
    CONSTRAINT id_usuario_encuesta_fk
    FOREIGN KEY (id_usuario_encuesta) REFERENCES Usuario(id_usuario) ON DELETE CASCADE
);

CREATE TABLE Pregunta(
    id_pregunta SERIAL PRIMARY KEY,
    id_encuesta_pregunta INTEGER,
    titulo_pregunta TEXT,
    opcion_1  TEXT,
    opcion_2  TEXT,
    opcion_3  TEXT,
    opcion_4  TEXT,
    CONSTRAINT id_encuesta_pregunta_fk
    FOREIGN KEY (id_encuesta_pregunta) REFERENCES Encuesta(id_encuesta) ON DELETE CASCADE
);
CREATE TABLE Respuesta(
    id_respuesta SERIAL PRIMARY KEY,
    id_pregunta_respuesta INTEGER,
    value_respuesta TEXT,
    CONSTRAINT id_pregunta_respuesta_fk
    FOREIGN KEY (id_pregunta_respuesta) REFERENCES Pregunta(id_pregunta) ON DELETE CASCADE
);
create table Contactanos(
	id_contac  SERIAL PRIMARY KEY,
	nombre_contac TEXT,
	correo_contact TEXT,
	telefono_contact TEXT,
	mensaje_contact TEXT,
	problema_contact TEXT
);