const { Router } = require('express')
const routes = Router()
const { verTodasLasEncuestas, buscarEncuestasPorTitulo, misEncuestas } = require('../controllers/encuestas.controller')

routes.get('/', verTodasLasEncuestas);
routes.get('/search/:titulo', buscarEncuestasPorTitulo);
routes.get('/mis-encuestas/:id', misEncuestas);


module.exports = routes;