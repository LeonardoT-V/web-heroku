const { Router } = require('express')
const routes = Router()
const { getUser, postUser, getUserByMail, deleteUser, modificarUser } = require('../controllers/usuarios.controller')

routes.get('/', getUser);
routes.post('/', postUser);
routes.get('/:id', getUserByMail)
routes.delete('/:id', deleteUser)
routes.put('/:id', modificarUser)

module.exports = routes