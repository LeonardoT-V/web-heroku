const { Router } = require('express')
const routes = Router()
const { getUserByMail } = require('../controllers/auth.controller')


routes.post('/', getUserByMail);


module.exports = routes