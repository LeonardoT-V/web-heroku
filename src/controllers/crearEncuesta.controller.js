const pool = require('../db/coneccionDB');

const ingresarEncuesta = async (req, res) => {
    const { titulo, descripcion, estado, usuario } = req.body;

    const response = await pool.query(`insert into Encuesta 
    (id_usuario_encuesta,titulo_encuesta,descripcion_encuesta,estado_encuesta)
    values($1,$2,$3,$4) RETURNING id_encuesta`, [usuario, titulo, descripcion, estado])
    res.status(200).json({ msg: 'Encuesta creada', id_encuesta: response.rows[0].id_encuesta })
}
const publicarEncuesta = async (req, res) => {
    const id_encuesta = req.params.id;
    console.log(id_encuesta);
    const response = await pool.query(`UPDATE Encuesta SET 
	estado_encuesta=true,
    fecha_publicado_encuesta=now()
    WHERE id_encuesta=$1`, [id_encuesta])
    res.status(200).json({ msg: 'Encuesta publicada: Actualizar si no reflejan cambios' })
}
const modificarEncuesta = async (req, res) => {
    const id_encuesta = req.params.id;
    const { titulo, descripcion, estado } = req.body;
    const response = await pool.query(`UPDATE Encuesta SET 
    titulo_encuesta=$1,descripcion_encuesta=$2,estado_encuesta=$3
    WHERE id_encuesta=$4`, [titulo, descripcion, estado, id_encuesta])
    console.log(id_encuesta);
    res.status(200).json({ msg: 'encuesta actualizada' })
}

const eliminarEncuesta = async (req, res) => {
    const id_encuesta = req.params.id;
    const response = await pool.query('DELETE from Encuesta where id_encuesta=$1', [id_encuesta])
    res.status(200).json({ msg: 'Encuesta elminada correctamente: Actualizar si no reflejan cambios' })
}

const conseguirEncuestaActual = async (req, res) => {
    const id_encuesta = req.params.id;
    const response = await pool.query('SELECT * FROM Encuesta WHERE id_encuesta = $1;', [id_encuesta])
    body = response.rows[0]
    res.status(200).json(body)
}
module.exports = {
    ingresarEncuesta,
    modificarEncuesta,
    eliminarEncuesta,
    publicarEncuesta,
    conseguirEncuestaActual
}