const pool = require('../db/coneccionDB');

const verTodasLasEncuestas = async (req, res) => {
    const response = await pool.query(`select 
    id_encuesta,
        fecha_publicado_encuesta,
        titulo_encuesta,
        descripcion_encuesta,
        Usuario.nombre_usuario,
        Usuario.email_usuario
    from Encuesta
    INNER JOIN Usuario on
    Encuesta.id_usuario_encuesta = Usuario.id_usuario
    where estado_encuesta = true
    ORDER BY fecha_publicado_encuesta DESC; `)
    const body = response.rows
    res.status(200).json(body)
}
const buscarEncuestasPorTitulo = async (req, res) => {
    const titulo = req.params.titulo;

    const response = await pool.query(`SELECT 
    id_encuesta,
        fecha_publicado_encuesta,
        titulo_encuesta,
        descripcion_encuesta,
        Usuario.nombre_usuario,
        Usuario.email_usuario
    FROM Encuesta
    INNER JOIN Usuario on
    Encuesta.id_usuario_encuesta = Usuario.id_usuario
    WHERE estado_encuesta = true AND titulo_encuesta ~* $1
    ORDER BY fecha_publicado_encuesta DESC;`, [titulo])
    const body = response.rows
    res.status(200).json(body)
}

const misEncuestas = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query(`SELECT * FROM
    Encuesta
    WHERE id_usuario_encuesta = $1
    ORDER BY fecha_publicado_encuesta DESC;`, [id]);
    const body = response.rows;
    res.status(200).json(body)
}

module.exports = {
    verTodasLasEncuestas,
    buscarEncuestasPorTitulo,
    misEncuestas
}