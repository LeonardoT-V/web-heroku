const pool = require('../db/coneccionDB');

const formularioContactanos = async (req, res) => {
    const { nombre, correo, telefono, mensaje, problema } = req.body;
    const response = await pool.query(`
    INSERT INTO Contactanos(
        nombre_contac,correo_contact,telefono_contact,mensaje_contact,problema_contact
    )values($1,$2,$3,$4,$5)`, [nombre, correo, telefono, mensaje, problema])
    res.status(200).json({ msg: 'Enviado satisfactoriamente' })
}

module.exports = {
    formularioContactanos
}