const pool = require('../db/coneccionDB');

const postUser = async (req, res) => {
    const { nombre, email, password } = req.body;
    /* console.log(nombre, email, password); */
    try {
        const verificar = await pool.query(`select email_usuario from Usuario where email_usuario=$1`, [email])
        if (verificar.rowCount >= 1) {
            return res.status(400).json({ msg: 'correo se encuentra usado' })
        }

        const response = await pool.query(`insert into Usuario 
        (nombre_usuario,email_usuario,password_usuario)
        values($1,$2,$3) RETURNING id_usuario `, [nombre, email, password])
        const { id_usuario } = response.rows[0]
        res.status(200).json({
            message: 'usuario creado correctamente',
            body: {
                nombre_usuario: nombre,
                email,
                id_usuario
            }
        })
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'correo se encuentra usado' })
    }

}
module.exports = {
    postUser
}