const pool = require('../db/coneccionDB');

const getUser = async (req, res) => {
    const user = await pool.query('Select * from usuarios')
    console.log(user.rows);
    res.status(200).json(user.rows)
}
const postUser = async (req, res) => {
    const { nombre, email, password } = req.body;
    console.log(nombre, email, password);
    const response = await pool.query('insert into usuarios (nombre,email,password)values($1,$2,$3)', [nombre, email, password])
    console.log(response.rows);
    /* console.log(req.body); */
    res.json({
        message: 'usuario creado correctamente',
        body: {
            user: { nombre, email, password }
        }
    })
}

const getUserByMail = async (req, res) => {
    /* res.send('User ID ' + req.params.id); */
    /*  const id = req.params.id;
     const response = await pool.query('select * from usuarios where id=$1', [id])
     res.json(response.rows) */
    const id = req.params.id;
    const response = await pool.query('select * from usuarios where email=$1', [id])
    if (response.rowCount == 0) {
        res.json({ msg: 'no encontrado' })
        return;
    }
    /* let usuario = {};
    response.rows.map(items => {
        usuario = items
    })
    const { password } = usuario
 */
    res.json(response.rows)
}

const deleteUser = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('delete from usuarios where id = $1', [id]);
    console.log(response);
    res.json(`User ${id} fue eliminado correctamente`)
}

const modificarUser = async (req, res) => {
    const id = req.params.id;
    const { nombre, email, password } = req.body;
    /* console.log(id, nombre, email, password); */
    const response = await pool.query('UPDATE usuarios set nombre = $1,email=$2,password=$3 where id=$4',
        [nombre, email, password, id])
    console.log(response);

    res.json('usuario actualizado correctamente')
}
module.exports = {
    getUser, postUser, getUserByMail, deleteUser, modificarUser
}