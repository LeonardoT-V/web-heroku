const pool = require('../db/coneccionDB');

const getUserByMail = async (req, res) => {

    const { email, pass } = req.body;

    const { email, pass } = req.body;
    const response = await pool.query('select * from Usuario where email_usuario=$1', [email])
    if (response.rowCount === 0) {
        return res.status(400).json({ msg: 'correo incorrecto' })
    }
    let usuarioConsultado = response.rows[0];
    const { password_usuario, nombre_usuario, id_usuario } = usuarioConsultado

    if (password_usuario !== pass) {
        return res.status(400).json({ msg: 'contraseña incorrecta' })
    }

    res.status(200).json({
        msg: 'logeado correctamente',
        body: { nombre_usuario, email, id_usuario }
    })
}

module.exports = {
    getUserByMail
}